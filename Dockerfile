FROM node:lts-alpine

ENV APP_DIR /app/
WORKDIR ${APP_DIR}

COPY . ./
RUN npm install

ENV HOST 0.0.0.0

EXPOSE 3000

CMD ["npm", "run", "prod"]
