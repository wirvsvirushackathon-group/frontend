# wirvsvirus-game

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

### using docker-compose

```bash
# in production
$ NODE_ENV=production docker-compose up -d --build

# in production
$ NODE_ENV=development docker-compose up -d --build
```
