import userRepository from '../services/api/repositories/userRepository.js'
import challengeRepository from '../services/api/repositories/challengeRepository.js'
import solutionRepository from '../services/api/repositories/solutionRepository.js'

export default ({ $axios }, inject) => {
  inject('userRepository', userRepository($axios))
  inject('challengeRepository', challengeRepository($axios))
  inject('solutionRepository', solutionRepository($axios))
}

/**
 * Add type hints for IDE support
 * @var {userRepository} $userRepository
 * @var {challengeRepository} $challengeRepository
 * @var {solutionRepository} $solutionRepository
 */
