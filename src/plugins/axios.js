export default ({ $axios, redirect }) => {
  $axios.setBaseURL(`//${process.env.BACKEND_EXTERNAL_HOST}/${process.env.BACKEND_API_ROUTE}/`)

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    if (code === 401) {
      redirect('/profile/login')
    }
  })
}
