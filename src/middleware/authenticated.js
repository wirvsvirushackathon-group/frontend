export default function ({ app, route, redirect }) {
  // Get authentication status
  if (route.path === '/') {
    const dateNow = Date.now()
    const dateStorage = localStorage.getItem('app:opened')
    const twoDays = (24 * 3600) * 2
    // If the time, the user last opened the app is two days ago,
    // show splash screen,
    // otherwise redirect to /welcome
    if (
      dateStorage !== null &&
      ((dateNow - dateStorage) < twoDays)
    ) {
      redirect('/welcome')
    }
  }
}
