export default $axios => ({
  async register (email, username, password) {
    return await $axios.post('/users/register', { email, password, username })
  },
  async login (email, password) {
    return await $axios.post('/users/login', { email, password })
  },
  async getUserInfo () {
    return await $axios.get('/users/dashboard')
  }
})
