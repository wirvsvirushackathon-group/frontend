export default $axios => ({
  async new (title, description) {
    return await $axios.post('/solution', { name: title, text: description })
  },
  async get (solution) {
    return await $axios.get(`/challenges/${solution}`)
  },
  async getByChallenge (challenge) {
    return await $axios.get(`/challenges/${challenge}/solutions`)
  }
})
