export default $axios => ({
  async new (title, description) {
    return await $axios.post('/challenges', { name: title, text: description })
  },
  async get (challenge) {
    return await $axios.get(`/challenges/${challenge}?solutions=true`)
  },
  async getAll () {
    return await $axios.get('/challenges?solutions=true')
  },
  async getAllSolutions () {
    return await $axios.get('/challenge-solutions')
  },
  async participate (challenge, solution) {
    const formData = new FormData()
    formData.append('file', solution)
    return await $axios.post(
      `/challenges/${challenge}/solutions`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    )
  }
})
