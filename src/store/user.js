export const state = () => ({
  email: null,
  username: null,
  points: 0
})

export const actions = {
  async register ({ commit }, { email, password, username }) {
    const response = await this.$userRepository.register(email, username, password)
    this.$auth.setUserToken(response.data.token)
    return response
  },
  async fetchUserInfo ({ commit }) {
    const response = await this.$userRepository.getUserInfo()
    commit('SET_FIELD', { key: 'points', value: response.data.points })
    commit('SET_FIELD', { key: 'username', value: response.data.username })
    return response
  }
}

export const mutations = {
  SET_FIELD (state, { key, value }) {
    state[key] = value
  }
}
