export const state = () => ({
  selectedChallenge: {},
  challengeList: {},
  challengeListFull: {}
})

export const actions = {
  async fetchChallenge ({ commit }, { challenge }) {
    const response = await this.$challengeRepository.challenge(challenge)
    commit('SET_FIELD', { key: 'selectedChallenge', value: response.data.challenge })
    return response
  },
  async fetchAllChallenges ({ commit }) {
    const response = await this.$challengeRepository.getAll()
    commit('SET_FIELD', { key: 'challengeList', value: response.data.challenges })
    return response
  },
  async fetchAllChallengesWithSolutions ({ commit }) {
    const response = await this.$challengeRepository.getAllSolutions()
    commit('SET_FIELD', { key: 'challengeListFull', value: response.data.challenges })
    return response
  }
}

export const mutations = {
  SET_FIELD (state, { key, value }) {
    state[key] = value
  }
}
