require('dotenv').config()

export default {
  mode: 'spa',
  /*
   ** Define root directory and override default
   ** See https://nuxtjs.org/api/configuration-rootdir
   */
  rootDir: '.',
  /*
   ** Define source directory and override default
   ** See https://nuxtjs.org/api/configuration-srcdir
   */
  srcDir: 'src',
  /*
   ** Define custom server configuration
   ** See https://nuxtjs.org/api/configuration-server
   */
  server: {
    host: process.env.FRONTEND_INTERNAL_HOST || '0.0.0.0',
    port: process.env.FRONTEND_INTERNAL_PORT || 3000,
    timing: false
  },
  head: {
    htmlAttrs: {
      lang: 'de'
    },
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Inter:300,500,600,700,800|Pacifico&display=swap' }
    ]
  },
  serverMiddleware: [],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#00CCAA' },
  styleResources: {
    scss: [
      './assets/variables.scss',
      './assets/styles/utils/_functions.scss',
      './assets/styles/utils/_mixins.scss'
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    './assets/styles/transitions.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios.js',
    '~/plugins/repositories.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/pwa',
    ['@nuxtjs/dotenv', { filename: '.env.' + (process.env.NODE_ENV || 'development'), path: '.' }],
    ['nuxt-env', { keys: ['WHOAMI', 'BACKEND_API_ROUTE', 'BACKEND_EXTERNAL_HOST'] }]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/style-resources',
    '@nuxtjs/toast',
    '@nuxtjs/auth'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},

  /**
   * Router config
   * https://nuxtjs.org/api/configuration-router/
   */
  router: {
    // Require authorization for every page
    // Can be disabled by setting `auth: false` in the pages' options
    middleware: ['authenticated', 'auth']
  },

  /**
   * Auth module configuration
   * https://auth.nuxtjs.org/api/options.html
   */
  auth: {
    localStorage: false,
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/users/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          user: false
        }
      }
    },
    redirect: {
      login: '/profile/login',
      logout: '/profile/login',
      home: '/challenges'
    }
  },
  toast: {
    position: 'top-center',
    duration: 3000
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
